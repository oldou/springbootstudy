package com.oldou;


import com.oldou.config.RedisConfig;
import com.oldou.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;


@SpringBootTest
class Springboot10RedisApplicationTests {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Test
    void contextLoads() {

        /**使用redisTemplate操作不同的数据类型,api和指令是一样的
         * opsForValue():操作字符串 类似String
         * opsForList():操作List 类似List
         * opsForHash():操作Hash
         * opsForZSet()
         * opsForGeo()
         * opsForHyperLogLog()
         * 除了基本的操作，我们常用的方法都可以直接通过redisTemplate操作，比如事务和基本的CURD
         */
        redisTemplate.opsForValue().set("MyKey1","hello redisTemplate！");
        System.out.println(redisTemplate.opsForValue().get("MyKey1"));

        /** 获取Redis的连接对象
         *  RedisConnection connection = redisTemplate.getConnectionFactory().getConnection();
         *  connection.flushAll();
         *  connection.flushDb();
         */


    }

    @Test
    public void test(){
        //真实开发中一般都使用Json来传递对象
        User user = new User("oldou","123");
        //ObjectMapper objectMapper = new ObjectMapper();

        redisTemplate.opsForValue().set("user",user);
        System.out.println(redisTemplate.opsForValue().get(user));
    }


}
