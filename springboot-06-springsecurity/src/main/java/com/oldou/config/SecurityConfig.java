package com.oldou.config;

import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    //授权
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        /** 实现首页所有人可以访问，功能页只有对应有权限的人才能访问
         *  请求授权的规则
         */
        http.authorizeRequests()
                .antMatchers("/").permitAll()   //实现所有人可以访问请求 /
                .antMatchers("/level1/**").hasRole("vip1")
                .antMatchers("/level2/**").hasRole("vip2")
                .antMatchers("/level3/**").hasRole("vip3");

        //没有权限默认会跳转到登录页面(/login)，这里需要开启登录的页面。
        //定制登录页面 loginPage("/toLogin")
        http.formLogin().loginPage("/toLogin")
               // .usernameParameter("uname")
               // .passwordParameter("pwd")
                .loginProcessingUrl("/login");

        //防止网站的跨站请求伪造CSRF 注销是GET请求
        http.csrf().disable(); //关闭csrf功能，登录失败可能存在的原因

        //开启了注销功能,注销成功之后跳转到首页
        http.logout().logoutSuccessUrl("/");

        //开启记住我的功能，使用的是cookie，默认保存时间是两周，我们还可以使用自定义的记住我
        http.rememberMe().rememberMeParameter("rememberMe");

    }

    /** 认证   springboot 2.1.x 可以直接使用
     *  这里如果不设置密码的编码，就会报服务器错误，同时会说PasswordEncoder(密码编码)为null，因此要设置编码格式
     *  在Spring Security中有很多加密的方式，还可以使用Java原生的MD5加盐的方式，这里我们使用框架推荐的加密方式
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication().passwordEncoder(new BCryptPasswordEncoder())
                .withUser("oldou").password(new BCryptPasswordEncoder().encode("123456")).roles("vip1","vip2")
                .and()
                .withUser("root").password(new BCryptPasswordEncoder().encode("123456")).roles("vip1","vip2","vip3")
                .and()
                .withUser("guest").password(new BCryptPasswordEncoder().encode("123456")).roles("vip1");
    }



}
