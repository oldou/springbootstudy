package com.oldou.service.impl;

import com.oldou.dao.UserDao;
import com.oldou.pojo.User;
import com.oldou.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;

    @Override
    public void save(User user) {
        this.userDao.save(user);
    }

    @Override
    public void delete(int id) {
        this.userDao.delete(id);
    }

    @Override
    public List<Map<String, Object>> QueryAll() {
        return this.userDao.QueryAll();
    }

    @Override
    public User QueryUserByID(int id) {
        return this.userDao.QueryUserByID(id);
    }

    @Override
    public void updateUser(User user) {
        this.userDao.updateUser(user);
    }
}
