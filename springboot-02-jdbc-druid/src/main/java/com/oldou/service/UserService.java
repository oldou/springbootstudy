package com.oldou.service;

import com.oldou.pojo.User;

import java.util.List;
import java.util.Map;

public interface UserService {
    /**
     * 保存用户
     * @return
     */
    public void save(User user);
    /**
     * 根据ID删除用户
     */
    public void delete(int id);
    /**
     * 查询全部用户
     * @return
     */
    public List<Map<String, Object>> QueryAll();
    /**
     * 根据ID查询用户
     * @return
     */
    public User QueryUserByID(int id);

    /**
     * 更新用户
     */
    public void updateUser(User user);
}
