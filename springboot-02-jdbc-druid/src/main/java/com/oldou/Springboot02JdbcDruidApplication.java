package com.oldou;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springboot02JdbcDruidApplication {

    public static void main(String[] args) {
        SpringApplication.run(Springboot02JdbcDruidApplication.class, args);
    }

}
