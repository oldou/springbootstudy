package com.oldou.controller;

import com.oldou.pojo.User;
import com.oldou.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class UserController {
    @Autowired
    private UserService userService;
    //查询user表中所有数据
    //List 中的1个 Map 对应数据库的 1行数据
    //Map 中的 key 对应数据库的字段名，value 对应数据库的字段值
    @GetMapping("/list")
    public List<Map<String, Object>> queryAllUser(){
        List<Map<String, Object>> list = userService.QueryAll();
        return list;
    }
    //根据ID查询用户
    @GetMapping("/query/{id}")
    public User queryUserByID(@PathVariable("id") int id){
        User user = userService.QueryUserByID(id);
        return user;
    }
    //根据ID删除用户
    @RequestMapping("/delete/{id}")
    public String deleteUser(@PathVariable("id") int id){
        userService.delete(id);
        return "删除用户成功！！";
    }

    //增加用户
    @RequestMapping("/add")
    public String addUser(){
        userService.save(new User(6,"ppp","1232"));
        return "增加用户成功";
    }

    //根据ID更新用户
    @RequestMapping("/update/{id}")
    public String updateUser(@PathVariable("id") int id){
        userService.updateUser(new User(id,"ppp","999"));
        return "更新用户成功";
    }
}
