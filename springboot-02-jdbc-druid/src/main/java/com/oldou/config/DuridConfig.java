package com.oldou.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class DuridConfig {
    /* 将yml文件和这个文件绑定起来
      将自定义的 Druid数据源添加到容器中，不再让 Spring Boot 自动创建
      绑定全局配置文件中的 druid 数据源属性到 com.alibaba.druid.pool.DruidDataSource从而让它们生效
      @ConfigurationProperties(prefix = "spring.datasource")：作用就是将 全局配置文件中
      前缀为 spring.datasource的属性值注入到 com.alibaba.druid.pool.DruidDataSource 的同名参数中
    */
    @ConfigurationProperties(prefix = "spring.datasource")
    @Bean
    public DataSource druidDataSource(){
        return new DruidDataSource();
    }

    /** 后台监控    这个就相当于web.xml文件  ServletRegistrationBean
     * 配置 Druid 监控管理后台的Servlet；     DynamicRegistrationBean
     * 内置 Servlet 容器时没有web.xml文件，所以使用 Spring Boot 的注册 Servlet 方式
     * 替代web.xml的方法：在ServletRegistrationBean中new一个新的方法就可以了
     */
    @Bean
    public ServletRegistrationBean statViewServlet(){
        //配置进入后台监控的路径(固定代码)
        ServletRegistrationBean<StatViewServlet> bean =
                new ServletRegistrationBean<>(new StatViewServlet(), "/druid/*");
        //后台有人登录，需要账号密码(固定)
        Map<String, String> initParameters = new HashMap<>();

        //添加配置--注意：登录的Key是固定的 loginUsername  loginPassword
        initParameters.put("loginUsername","admin");
        initParameters.put("loginPassword","123456");

        //允许谁可以访问--如果为空表示谁都可以，localhost表示本机，IP地址
        initParameters.put("allow","");
        //initParams.put("allow", "localhost")：表示只有本机可以访问
        //initParams.put("allow", "")：为空或者为null时，表示允许所有访问
        //禁止谁能访问 initParameters.put("oldou","192.168.15.131");

        //设置初始化参数
        bean.setInitParameters(initParameters);
        return bean;
    }

    /** 配置过滤器 filter   可以去查看WebStatFilter下有哪些东西可以配置
     */
    @Bean
    public FilterRegistrationBean webStatFilter(){
        FilterRegistrationBean bean = new FilterRegistrationBean();
        bean.setFilter(new WebStatFilter());

        //可以过滤哪些请求？
        Map<String, String> initParameters = new HashMap<>();

        //以下不进行统计
        initParameters.put("exclusions","*.js,*.css,/druid/*");

        bean.setInitParameters(initParameters);
        return bean;
    }





}
