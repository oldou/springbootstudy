# springbootstudy

#### Description
该仓库为学习SpringBoot时的源码，里面包含了SpringBoot、SpringBoot整合Shiro、SpringBoot整合Spring Security、SpringBoot整合MyBatis、SpringBoot整合JDBC、SpringBoot整合Redis、SpringBoot整合RabbitMQ等等之类的源码以及教程，如果该教程对你有所帮助，还希望点个start支持一下，谢谢。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
