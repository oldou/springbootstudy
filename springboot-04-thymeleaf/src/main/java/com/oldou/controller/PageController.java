package com.oldou.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Date;

@Controller
public class PageController {

    @GetMapping("/show")
    public String showPage(Model model){
        model.addAttribute("msg","Hello Thymeleaf");
        model.addAttribute("date",new Date());

        return "index";
    }

}
