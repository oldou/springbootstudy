package com.oldou.controller;

import com.oldou.pojo.User;
import com.oldou.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


@Controller
public class UserConyroller {
    @Autowired
    private UserService userService;
    //跳转到主页
    @RequestMapping({"/","/index","/index.html"})
    public String index(){
        return "index";
    }

    @RequestMapping("/user/findUserAll")
    public String getAllUser(Model model){
        List<User> list = userService.queryAllUser();
        model.addAttribute("list",list);
        return "showUser";
    }
    @RequestMapping("/user/toAddUser")
    public String toAddUser(){
        return "addUser";
    }
    @RequestMapping("/user/addUser")
    public String addUser(User user){
        userService.addUser(user);
        return "ok";
    }

    @RequestMapping("/user/preUpdateUser")
    public String preUpdateUser(int id,Model model){
        User user = userService.queryUserByID(id);
        model.addAttribute(user);
        return "updateUser";
    }

    @RequestMapping("/user/updateUser")
    public String updateUser(User user){
        int flag = userService.updateUser(user);
        if(flag>0){
            return "ok";
        }else {
            return "error";
        }
    }

    @RequestMapping("/user/deleteUser")
    public String deleteUser(int id){
        int i = userService.deleteUser(id);
        if(i>0){
            return "ok";
        }else {
            return "error";
        }

    }
}
