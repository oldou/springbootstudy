package com.oldou;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springboot03MybatisDruidApplication {

    public static void main(String[] args) {
        SpringApplication.run(Springboot03MybatisDruidApplication.class, args);
    }

}
