package com.oldou.mapper;

import com.oldou.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface UserMapper {
    //查询全部用户
    List<User> queryAllUser();
    //根据ID查询用户
    User queryUserByID(int id);
    //更新用户信息
    int updateUser(User user);
    //删除用户
    int deleteUser(int id);
    //增加用户
    void addUser(User user);
}
